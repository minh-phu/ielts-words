#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_com_ielts_vocabulary_be_MyApplication_getSecretKey(
    JNIEnv *env,
    jobject) {
  std::string text = "https://play.google.com/store/apps/dev?id=5035445692539325693";
  text = text.substr(text.length() - 16, text.length());
  return env->NewStringUTF(text.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_ielts_vocabulary_be_utils_Utils_getUtilsSecretKey(
    JNIEnv *env,
    jobject) {
  std::string text = "33ff818a64b26ebc8a8dca00b60cf2ce";
  return env->NewStringUTF(text.c_str());
}