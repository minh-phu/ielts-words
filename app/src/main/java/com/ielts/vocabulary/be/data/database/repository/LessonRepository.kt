package com.ielts.vocabulary.be.data.database.repository

import com.ielts.vocabulary.be.data.database.daos.LessonDao

class LessonRepository private constructor(private val lessonDao: LessonDao) {

    fun getListLesson() = lessonDao.getAllLesson()

    companion object {

        @Volatile
        private var instance: LessonRepository? = null

        fun getInstance(lessonDao: LessonDao) =
            instance ?: synchronized(this) {
                instance ?: LessonRepository(lessonDao).also { instance = it }
            }
    }
}
