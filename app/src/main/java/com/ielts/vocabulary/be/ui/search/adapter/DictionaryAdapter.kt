package com.ielts.vocabulary.be.ui.search.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.l4digital.fastscroll.FastScroller
import kotlinx.android.synthetic.main.item_dictionary.view.*
import com.ielts.vocabulary.be.R
import com.ielts.vocabulary.be.data.database.entities.Word
import java.util.*


class DictionaryAdapter(vocabularyList: List<Word>, private val listener: ItemDictionaryClick) :
    RecyclerView.Adapter<DictionaryAdapter.DictionaryViewHolder>(), FastScroller.SectionIndexer {

    private val vocabularyList: MutableList<Word>

    init {
        this.vocabularyList = ArrayList(vocabularyList)
    }

    override fun getSectionText(position: Int): CharSequence {
        val item = vocabularyList[position]
        return item.enDecrypt[0].toString()
    }

    override fun getItemCount(): Int = vocabularyList.size

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): DictionaryViewHolder {
        val itemView = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_dictionary, viewGroup, false)
        return DictionaryViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: DictionaryViewHolder, position: Int) {
        val vocabulary = vocabularyList[position]
        holder.itemView.tvWord.text = vocabulary.enDecrypt
        holder.itemView.tvDescription.text = vocabulary.descDecrypt
    }

    fun animateTo(filteredModelList: List<Word>) {
        applyAndAnimateRemovals(filteredModelList)
        applyAndAnimateAdditions(filteredModelList)
        applyAndAnimateMovedItems(filteredModelList)
    }

    private fun applyAndAnimateRemovals(newModels: List<Word>) {
        for (i in vocabularyList.indices.reversed()) {
            val model = vocabularyList[i]
            if (!newModels.contains(model)) {
                removeItem(i)
            }
        }
    }

    private fun applyAndAnimateAdditions(newModels: List<Word>) {
        var i = 0
        val count = newModels.size
        while (i < count) {
            val model = newModels[i]
            if (!vocabularyList.contains(model)) {
                addItem(i, model)
            }
            i++
        }
    }

    private fun applyAndAnimateMovedItems(newModels: List<Word>) {
        for (toPosition in newModels.indices.reversed()) {
            val model = newModels[toPosition]
            val fromPosition = vocabularyList.indexOf(model)
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition)
            }
        }
    }

    private fun removeItem(position: Int) {
        vocabularyList.removeAt(position)
        notifyItemRemoved(position)
    }

    private fun addItem(position: Int, model: Word) {
        vocabularyList.add(position, model)
        notifyItemInserted(position)
    }

    private fun moveItem(fromPosition: Int, toPosition: Int) {
        val model = vocabularyList.removeAt(fromPosition)
        vocabularyList.add(toPosition, model)
        notifyItemMoved(fromPosition, toPosition)
    }

    inner class DictionaryViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        init {
            v.setOnClickListener {
                listener.onItemDictionaryClick(vocabularyList[adapterPosition])
            }
        }
    }

    interface ItemDictionaryClick {
        fun onItemDictionaryClick(word: Word)
    }
}
