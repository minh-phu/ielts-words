package com.ielts.vocabulary.be.ui.promoteapp

import android.content.Context
import com.ielts.vocabulary.be.R

class AppPromote(
        var appName: String,
        var appIcon: Int,
        var appUrl: String,
        var isMoreApp: Boolean = false
) {
    companion object {
        fun getListPromoteApp(context: Context): ArrayList<AppPromote> {
            val listMoreApp = ArrayList<AppPromote>()

            listMoreApp.add(
                    AppPromote(
                            "IELTS Essay",
                            R.drawable.essay_256,
                            context.getString(R.string.package_essay)
                    )
            )

            listMoreApp.add(
                    AppPromote(
                            "IELTS Grammar Test",
                            R.drawable.ielts_test_256,
                            context.getString(R.string.package_ielts_test)
                    )
            )

            listMoreApp.add(
                    AppPromote(
                            "English Grammar",
                            R.drawable.grammar_icon_256,
                            context.getString(R.string.package_grammar)
                    )
            )

            listMoreApp.add(
                    AppPromote(
                            "Vocabulary Builder",
                            R.drawable.voca_builder_256,
                            context.getString(R.string.package_vocabulary_builder)
                    )
            )

            listMoreApp.add(
                    AppPromote(
                            "More App",
                            R.drawable.icon_dev,
                            context.getString(R.string.dev_page), true
                    )
            )
            return listMoreApp
        }
    }
}