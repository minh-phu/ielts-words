package com.ielts.vocabulary.be.utils

import android.util.Base64
import com.scottyab.aescrypt.AESCrypt
import java.security.GeneralSecurityException
import javax.crypto.spec.SecretKeySpec

class CipherHelper(text: String? = null) {

    companion object {
        private lateinit var ivParam: String
        private lateinit var secretKeySpec: SecretKeySpec

        fun encrypt(str: String): String {
            try {
                val mess = AESCrypt.encrypt(secretKeySpec, ivParam.toByteArray(), str.toByteArray())
                return Base64.encodeToString(mess, Base64.DEFAULT).trim()

            } catch (e: GeneralSecurityException) {
                e.printStackTrace()
            }
            return ""
        }

        fun decrypt(str: String): String {
            try {
                val mess = Base64.decode(str, Base64.DEFAULT)
                val res = AESCrypt.decrypt(secretKeySpec, ivParam.toByteArray(), mess)
                return String(res)
            } catch (e: GeneralSecurityException) {
                e.printStackTrace()
            }
            return ""
        }
    }

    init {
        text?.let {
            ivParam = it
            secretKeySpec = SecretKeySpec(Utils.getUtilsSecretKey().toByteArray(), "AES")
        }
    }
}
