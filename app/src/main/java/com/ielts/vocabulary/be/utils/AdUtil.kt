package com.ielts.vocabulary.be.utils

import android.content.Context
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.facebook.ads.*
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.ielts.vocabulary.be.BuildConfig
import com.ielts.vocabulary.be.R
import java.util.*

object AdUtil {

    const val ID_TEST_FACEBOOK_ADS = "YOUR_PLACEMENT_ID"

    // return true if purchased
    fun checkPurchaseAd(context: Context): Boolean {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getBoolean(Constants.CHECK_PURCHASE, false)
    }

    fun showNativeAdsFbSmallInList(
        context: Context,
        nativeAdContainer: ViewGroup,
        idFbAds: Int,
        idGoogleAds: Int
    ) {
        if (checkPurchaseAd(context)) return
        val nativeAd: NativeBannerAd = when {
            BuildConfig.DEBUG -> NativeBannerAd(context, ID_TEST_FACEBOOK_ADS)
            else -> NativeBannerAd(context, context.getString(idFbAds))
        }
        nativeAd.loadAd()
        nativeAd.setAdListener(object : AdListener, NativeAdListener {
            override fun onMediaDownloaded(p0: Ad?) {}

            override fun onError(ad: Ad, error: AdError) {
                showNativeAdGgSmall(context, nativeAdContainer, idGoogleAds)
            }

            override fun onAdLoaded(ad: Ad) {
                val inflater = LayoutInflater.from(context)
                val adView = inflater.inflate(
                    R.layout.custom_layout_ads_fb_small,
                    nativeAdContainer,
                    false
                ) as LinearLayout
                nativeAdContainer.removeAllViews()
                nativeAdContainer.addView(adView)
                nativeAdContainer.visibility = View.VISIBLE

                nativeAd.unregisterView()
                val adChoicesContainer =
                    adView.findViewById<RelativeLayout>(R.id.ad_choices_container)
                val adChoicesView = AdChoicesView(context, nativeAd, true)
                adChoicesContainer.addView(adChoicesView, 0)

                val nativeAdIcon = adView.findViewById<AdIconView>(R.id.native_ad_icon)
                val nativeAdTitle = adView.findViewById<TextView>(R.id.native_ad_title)
                val nativeAdSocialContext =
                    adView.findViewById<TextView>(R.id.native_ad_social_context)
                val sponsoredLabel = adView.findViewById<TextView>(R.id.native_ad_sponsored_label)
                val nativeAdCallToAction =
                    adView.findViewById<TextView>(R.id.native_ad_call_to_action)

                nativeAdTitle.text = nativeAd.advertiserName
                nativeAdSocialContext.text = nativeAd.adSocialContext
                nativeAdCallToAction.visibility =
                        if (nativeAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE
                nativeAdCallToAction.text = nativeAd.adCallToAction
                sponsoredLabel.text = nativeAd.sponsoredTranslation

                val clickableViews = ArrayList<View>()
                clickableViews.add(nativeAdContainer)
                clickableViews.add(nativeAdIcon)
                clickableViews.add(nativeAdTitle)
                clickableViews.add(nativeAdCallToAction)

                nativeAd.registerViewForInteraction(
                    adView,
                    nativeAdIcon,
                    clickableViews
                )

            }

            override fun onAdClicked(ad: Ad) {
            }

            override fun onLoggingImpression(ad: Ad) {}
        })
    }

    fun showNativeAdFbSmall(
        context: Context,
        nativeAdContainer: ViewGroup,
        idFbAds: Int,
        idGoogleAds: Int
    ) {
        if (checkPurchaseAd(context)) return
        val nativeAd: NativeBannerAd = when {
            BuildConfig.DEBUG -> NativeBannerAd(context, ID_TEST_FACEBOOK_ADS)
            else -> NativeBannerAd(context, context.getString(idFbAds))
        }
        nativeAd.setAdListener(object : AdListener, NativeAdListener {
            override fun onMediaDownloaded(p0: Ad?) {
            }

            override fun onError(ad: Ad, error: AdError) {
                showNativeAdGgSmall(context, nativeAdContainer, idGoogleAds)
            }

            override fun onAdLoaded(ad: Ad) {

                val adView = NativeBannerAdView.render(
                    context,
                    nativeAd,
                    NativeBannerAdView.Type.HEIGHT_100
                )
                nativeAdContainer.removeAllViews()
                nativeAdContainer.addView(adView)
                nativeAdContainer.visibility = View.VISIBLE
            }

            override fun onAdClicked(ad: Ad) {

            }

            override fun onLoggingImpression(ad: Ad) {

            }

        })
        nativeAd.loadAd()
    }

    fun showNativeAdFbLarge(
        context: Context,
        nativeAdContainer: ViewGroup,
        idFbAds: Int,
        idGgAds: Int
    ) {
        if (checkPurchaseAd(context)) return
        val nativeAd: NativeAd = when {
            BuildConfig.DEBUG -> NativeAd(context, ID_TEST_FACEBOOK_ADS)
            else -> NativeAd(context, context.getString(idFbAds))
        }

        nativeAd.setAdListener(object : AdListener, NativeAdListener {
            override fun onMediaDownloaded(p0: Ad?) {

            }

            override fun onError(ad: Ad, error: AdError) {
                showNativeAdGgLarge(context, nativeAdContainer, idGgAds)
            }

            override fun onAdLoaded(ad: Ad) {

                nativeAdContainer.visibility = View.VISIBLE
                val inflater = LayoutInflater.from(context)
                val adView = inflater.inflate(
                    R.layout.custom_native_ad_fb,
                    nativeAdContainer,
                    false
                ) as LinearLayout
                nativeAdContainer.removeAllViews()
                nativeAdContainer.addView(adView)

                val nativeAdIcon = adView.findViewById<AdIconView>(R.id.native_ad_icon)
                val nativeAdTitle = adView.findViewById<TextView>(R.id.native_ad_title)
                val nativeAdMedia = adView.findViewById<MediaView>(R.id.native_ad_media)
                val nativeAdSocialContext =
                    adView.findViewById<TextView>(R.id.native_ad_social_context)
                val nativeAdBody = adView.findViewById<TextView>(R.id.native_ad_body)
                val nativeAdCallToAction =
                    adView.findViewById<TextView>(R.id.native_ad_call_to_action)

                nativeAdTitle.text = nativeAd.advertiserName
                nativeAdSocialContext.text = nativeAd.adSocialContext
                nativeAdBody.text = nativeAd.adBodyText
                nativeAdCallToAction.text = nativeAd.adCallToAction

                val adChoicesContainer =
                    adView.findViewById<View>(R.id.ad_choices_container) as LinearLayout
                val adChoicesView = AdChoicesView(context, nativeAd, true)
                adChoicesContainer.addView(adChoicesView)

                val clickableViews = ArrayList<View>()
                clickableViews.add(nativeAdTitle)
                clickableViews.add(nativeAdCallToAction)
                nativeAd.registerViewForInteraction(
                    nativeAdContainer,
                    nativeAdMedia,
                    nativeAdIcon,
                    clickableViews
                )
            }

            override fun onAdClicked(ad: Ad) {}

            override fun onLoggingImpression(ad: Ad) {
            }
        })
        nativeAd.loadAd()
    }

    fun showNativeAdGgLarge(context: Context, nativeAdContainer: ViewGroup, idGgAds: Int) {
        if (checkPurchaseAd(context)) return
        val adView = AdView(context)
        adView.adSize = AdSize.MEDIUM_RECTANGLE
        adView.adUnitId = context.getString(idGgAds)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
        adView.adListener = object : com.google.android.gms.ads.AdListener() {
            override fun onAdLoaded() {
                nativeAdContainer.removeAllViews()
                nativeAdContainer.addView(adView)
                nativeAdContainer.visibility = View.VISIBLE
            }
        }
    }

    fun showNativeAdGgSmall(context: Context, nativeAdContainer: ViewGroup, idGoogle: Int) {
        if (checkPurchaseAd(context)) return
        val adView = AdView(context)
        adView.adSize = AdSize.LARGE_BANNER
        adView.adUnitId = context.getString(idGoogle)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
        adView.adListener = object : com.google.android.gms.ads.AdListener() {
            override fun onAdLoaded() {
                nativeAdContainer.removeAllViews()
                nativeAdContainer.addView(adView)
                nativeAdContainer.visibility = View.VISIBLE
            }
        }
    }
}
