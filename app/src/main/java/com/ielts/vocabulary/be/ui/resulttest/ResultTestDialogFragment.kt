package com.ielts.vocabulary.be.ui.resulttest

import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_fragment_result_test.*
import com.ielts.vocabulary.be.R
import com.ielts.vocabulary.be.data.model.ResultPractice
import com.ielts.vocabulary.be.ui.base.BaseDialogFragment
import com.ielts.vocabulary.be.ui.resulttest.adapter.ResultTestAdapter
import java.util.*

class ResultTestDialogFragment : BaseDialogFragment() {

    lateinit var listResult: ArrayList<ResultPractice>
    private var isCompleteTest = false
    private var listener: OnCloseResultTestDialogFragment? = null

    override fun getFragmentLayout(): Int = R.layout.dialog_fragment_result_test

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialog)
        isCancelable = false
    }

    companion object {
        fun newInstance(
            listResult: ArrayList<ResultPractice>,
            isCompleteTest: Boolean,
            listener: OnCloseResultTestDialogFragment? = null
        ): ResultTestDialogFragment {
            val fragment = ResultTestDialogFragment()
            fragment.listResult = listResult
            fragment.isCompleteTest = isCompleteTest
            fragment.listener = listener
            return fragment
        }
    }

    override fun updateView() {
        when {
            isCompleteTest -> btnClose.text = getString(R.string.txt_home)
        }

        btnClose.setOnClickListener {
            dismiss()
        }

        activity?.let {
            listResult.sortWith(Comparator { x, y -> x.isUserChooseCorrect.compareTo(y.isUserChooseCorrect) })
            val resultAdapter = ResultTestAdapter(listResult, isCompleteTest)
            rcvResultPractice.adapter = resultAdapter
        }
    }

    override fun onDismiss(dialog: DialogInterface?) {
        if (isCompleteTest) {
            listener?.onCompleteTest()
        }
    }

    interface OnCloseResultTestDialogFragment {
        fun onCompleteTest()
    }
}