package com.ielts.vocabulary.be.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.ielts.vocabulary.be.utils.CipherHelper

@Entity
class Word {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "lesson_id")
    var lessonId: Int? = null

    @ColumnInfo(name = "en")
    var en: String? = null

    @ColumnInfo(name = "description")
    var description: String? = null

    @ColumnInfo(name = "exam")
    var exam: String? = null

    @Ignore
    var enEncrypt: String = ""
        get() = CipherHelper.encrypt(en!!)

    @Ignore
    var enDecrypt: String = ""
        get() = CipherHelper.decrypt(en!!)
    @Ignore
    var descDecrypt: String = ""
        get() = CipherHelper.decrypt(description!!)
    @Ignore
    var examDecrypt: String = ""
        get() = CipherHelper.decrypt(exam!!)
}