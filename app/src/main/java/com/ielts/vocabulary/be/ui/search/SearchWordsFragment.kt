package com.ielts.vocabulary.be.ui.search

import android.app.SearchManager
import android.content.Context
import android.view.Menu
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_search_word.*
import com.ielts.vocabulary.be.R
import com.ielts.vocabulary.be.data.database.AppDatabase
import com.ielts.vocabulary.be.data.database.entities.Word
import com.ielts.vocabulary.be.ui.base.BaseFragment
import com.ielts.vocabulary.be.ui.search.adapter.DictionaryAdapter
import com.ielts.vocabulary.be.utils.Utils
import com.ielts.vocabulary.be.utils.tts.TextToSpeechManager
import java.util.*
import java.util.Collections.sort

class SearchWordsFragment : BaseFragment(), SearchView.OnQueryTextListener,
    SearchView.OnCloseListener, DictionaryAdapter.ItemDictionaryClick {

    private lateinit var dictionaryAdapter: DictionaryAdapter

    private var wordList: List<Word>? = null

    override fun getFragmentLayout(): Int = R.layout.fragment_search_word

    override fun getMenuLayout(): Int = R.menu.menu_search

    override fun updateView() {
        val wordDao = AppDatabase.getInstance(requireContext()).wordDao()
        val words = wordDao.getAllWords()
        sort(
            words
        ) { s1, s2 -> s1.enDecrypt.compareTo(s2.enDecrypt, ignoreCase = true) }
        wordList = words
        dictionaryAdapter = DictionaryAdapter(wordList!!, this)
        rcvSearchWord.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
        rcvSearchWord.adapter = dictionaryAdapter
    }

    private fun filter(query: String) {
        val newQuery = query.toLowerCase()
        val filteredModelList = ArrayList<Word>()
        if (wordList != null) {
            for (i in wordList!!.indices) {
                val text = wordList!![i].enDecrypt.toLowerCase()
                if (text.contains(newQuery)) {
                    filteredModelList.add(wordList!![i])
                }
            }
            dictionaryAdapter.animateTo(filteredModelList)
        }
        rcvSearchWord.scrollToPosition(0)
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        val mSearchMenuItem = menu?.findItem(R.id.action_search)
        val searchView = mSearchMenuItem?.actionView as SearchView
        val searchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setIconifiedByDefault(true)
        searchView.isFocusable = true
        searchView.isIconified = false
        searchView.requestFocusFromTouch()
        searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
        searchView.setOnQueryTextListener(this)
        searchView.setOnCloseListener(this)
    }

    override fun onClose(): Boolean {
        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean = true

    override fun onQueryTextChange(newText: String?): Boolean {
        newText?.let { filter(it) }
        return true
    }

    override fun onDetach() {
        Utils.hideKeyboard(requireActivity())
        super.onDetach()
    }

    override fun onItemDictionaryClick(word: Word) {
        openDialog(word)
    }

    private var dialogDetailWord: AlertDialog? = null

    private lateinit var tvVocabulary: TextView
    private lateinit var tvMeaning: TextView
    private lateinit var tvExample: TextView
    private lateinit var btnSpeakVocabulary: AppCompatImageButton

    private fun openDialog(word: Word) {
        if (dialogDetailWord == null) {
            val dialogBuilder = AlertDialog.Builder(requireContext())
            val dialogView = View.inflate(requireContext(), R.layout.dialog_detail_word, null)
            dialogBuilder.setView(dialogView)

            tvVocabulary = dialogView.findViewById(R.id.tvVocabulary)
            tvMeaning = dialogView.findViewById(R.id.tvMeaning)
            tvExample = dialogView.findViewById(R.id.tvExample)
            btnSpeakVocabulary = dialogView.findViewById(R.id.btnSpeakVocabulary)

            dialogDetailWord = dialogBuilder.create()
            dialogDetailWord?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        }
        tvVocabulary.text = Utils.fromHtml(word.enDecrypt)
        tvMeaning.text = Utils.fromHtml("<b>Meaning</b> : " + word.descDecrypt)
        tvExample.text = Utils.fromHtml("<b>Example</b> : " + word.examDecrypt)
        btnSpeakVocabulary.setOnClickListener {
            TextToSpeechManager.playText(requireContext(), word.enDecrypt)
        }
        dialogDetailWord?.show()
    }
}