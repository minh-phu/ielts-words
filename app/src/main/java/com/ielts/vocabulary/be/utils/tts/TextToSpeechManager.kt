package com.ielts.vocabulary.be.utils.tts

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.speech.tts.TextToSpeech
import java.util.*

class TextToSpeechManager {
    companion object {

        private var tts: TextToSpeech? = null

        // must be init in Application
        fun getInstance(context: Context): TextToSpeech {
            if (tts == null) {
                tts = TextToSpeech(context, TextToSpeech.OnInitListener { status ->
                    if (status != TextToSpeech.ERROR) {
                        tts?.language = Locale.US
                    }
                })
            }
            return tts as TextToSpeech
        }

        fun playText(context: Context, text: String) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ttsGreater21(context, text)
            } else {
                ttsUnder20(context, text)
            }
        }

        @Suppress("DEPRECATION")
        private fun ttsUnder20(context: Context, text: String) {
            val map = HashMap<String, String>()
            map[TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID] = "MessageId"
            getInstance(context).speak(text, TextToSpeech.QUEUE_FLUSH, map)
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        private fun ttsGreater21(context: Context, text: String) {
            val utteranceId = this.hashCode().toString() + ""
            getInstance(context).speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId)
        }
    }
}