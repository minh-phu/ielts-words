package com.ielts.vocabulary.be.ui.lesson

import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import kotlinx.android.synthetic.main.fragment_lesson.*
import com.ielts.vocabulary.be.R
import com.ielts.vocabulary.be.data.database.entities.Lesson
import com.ielts.vocabulary.be.data.model.TestStyle
import com.ielts.vocabulary.be.data.viewmodel.LessonViewModel
import com.ielts.vocabulary.be.ui.base.BaseFragment
import com.ielts.vocabulary.be.ui.lesson.adapter.LessonAdapter
import com.ielts.vocabulary.be.utils.InjectorUtils

class LessonFragment : BaseFragment(), LessonAdapter.OnActionListLesson {

    override fun getFragmentLayout(): Int = R.layout.fragment_lesson

    override fun getMenuLayout(): Int = R.menu.menu_lesson

    override fun updateView() {
        val lessonAdapter = LessonAdapter(this@LessonFragment)
        rcvLesson.apply {
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
            adapter = lessonAdapter
            addItemDecoration(DividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL))
        }

        val factory = InjectorUtils.provideLessonViewModelFactory(requireActivity())
        val viewModel = ViewModelProviders.of(this, factory).get(LessonViewModel::class.java)

        viewModel.mListLesson.observe(viewLifecycleOwner, Observer { listLesson ->
            lessonAdapter.submitList(listLesson)
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_search -> {
                val directions = LessonFragmentDirections.toSearchWord()
                Navigation.findNavController(rcvLesson).navigate(directions)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onItemLessonClick(lesson: Lesson, style: TestStyle) {
        when (style) {
            TestStyle.VIEW_LESSON -> {
                val directions = LessonFragmentDirections.toListWordFragment(lesson.id)
                Navigation.findNavController(rcvLesson).navigate(directions)
            }
            TestStyle.CHOOSE_PRONOUNCE -> {
                val directions = LessonFragmentDirections.toListenTestOneFragment(lesson.id)
                Navigation.findNavController(rcvLesson).navigate(directions)
            }
            TestStyle.LISTEN_AND_CHOOSE -> {
                val directions = LessonFragmentDirections.toListenTestTwoFragment(lesson.id)
                Navigation.findNavController(rcvLesson).navigate(directions)
            }
            TestStyle.VOCABULARY_TEST -> {
                val directions =
                    LessonFragmentDirections.toRememberTestFragment(lesson.id, style.name)
                Navigation.findNavController(rcvLesson).navigate(directions)
            }
            TestStyle.MEANING_TEST -> {
                val directions =
                    LessonFragmentDirections.toRememberTestFragment(lesson.id, style.name)
                Navigation.findNavController(rcvLesson).navigate(directions)
            }
        }
    }

}

