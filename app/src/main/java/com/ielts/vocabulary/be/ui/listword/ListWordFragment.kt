package com.ielts.vocabulary.be.ui.listword

import androidx.viewpager.widget.ViewPager
import com.xw.repo.BubbleSeekBar
import kotlinx.android.synthetic.main.fragment_content_lesson.*
import com.ielts.vocabulary.be.R
import com.ielts.vocabulary.be.ui.base.BaseFragment
import com.ielts.vocabulary.be.ui.listword.adapter.ListWordPagerAdapter
import com.ielts.vocabulary.be.utils.InjectorUtils

class ListWordFragment : BaseFragment(), ViewPager.OnPageChangeListener,
    BubbleSeekBar.OnProgressChangedListener {

    override fun getFragmentLayout(): Int = R.layout.fragment_content_lesson

    override fun updateView() {
        vpFlashCard.addOnPageChangeListener(this)
        progress.onProgressChangedListener = this
        val lessonNumber = ListWordFragmentArgs.fromBundle(arguments!!).lessonNumber

        setTopTitle(getString(R.string.lesson_name, lessonNumber))
        val listWord =
            InjectorUtils.provideWordDao(requireContext()).getWordsByLessonId(lessonNumber)
        val flashCardAdapter = ListWordPagerAdapter(childFragmentManager, listWord)
        vpFlashCard.adapter = flashCardAdapter
        setSeekBar(listWord.size)
    }

    private fun setSeekBar(maxProgress: Int) {
        val minProgress = 1F
        progress.configBuilder
            .min(minProgress)
            .max(maxProgress.toFloat())
            .progress(1F)
            .sectionCount(maxProgress - minProgress.toInt())
            .build()
    }

    // seek bar
    override fun onProgressChanged(
        bubbleSeekBar: BubbleSeekBar?,
        progress: Int,
        progressFloat: Float,
        fromUser: Boolean
    ) {
    }

    override fun getProgressOnActionUp(
        bubbleSeekBar: BubbleSeekBar?,
        progress: Int,
        progressFloat: Float
    ) {
        vpFlashCard.currentItem = progressFloat.toInt() - 1
    }

    override fun getProgressOnFinally(
        bubbleSeekBar: BubbleSeekBar?,
        progress: Int,
        progressFloat: Float,
        fromUser: Boolean
    ) {
    }

    //view pager
    override fun onPageSelected(position: Int) {
        progress.setProgress(position.toFloat() + 1)
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

}

