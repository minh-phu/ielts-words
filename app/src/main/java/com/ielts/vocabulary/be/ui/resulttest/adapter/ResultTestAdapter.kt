package com.ielts.vocabulary.be.ui.resulttest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_result.view.*
import kotlinx.android.synthetic.main.item_result_header.view.*
import com.ielts.vocabulary.be.R
import com.ielts.vocabulary.be.data.model.ResultPractice
import com.ielts.vocabulary.be.utils.AdUtil
import com.ielts.vocabulary.be.utils.Utils

class ResultTestAdapter(
    private val resultTestList: ArrayList<ResultPractice>,
    private val isResult: Boolean
) : RecyclerView.Adapter<ResultTestAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val context = viewGroup.context
        if (viewType == 0) {
            val view =
                LayoutInflater.from(context).inflate(R.layout.item_result_header, viewGroup, false)
            val layoutNativeAds = view.findViewById<FrameLayout>(R.id.resultAdsContainer)
            if (isResult) {
                if (layoutNativeAds.childCount == 0) {
                    AdUtil.showNativeAdFbSmall(
                        context,
                        layoutNativeAds,
                        R.string.adsFb_ResultTestFragmentTop,
                        R.string.adsGg_ResultTestFragmentTop
                    )
                }
                val headerResult = view.findViewById<ConstraintLayout>(R.id.headerResult)
                headerResult.visibility = View.VISIBLE
            } else {
                if (layoutNativeAds.childCount == 0)
                    AdUtil.showNativeAdFbSmall(
                        context,
                        layoutNativeAds,
                        R.string.adsFb_ResultTestFragmentTop,
                        R.string.adsGg_ResultTestFragmentTop
                    )
            }
            when {
                score >= (resultTestList.size * 7 / 10) -> when (score) {
                    resultTestList.size -> view.txtResult.text = context.getString(R.string.perfect)
                    else -> view.txtResult.text = context.getString(R.string.passed)
                }
                else -> view.txtResult.text = context.getString(R.string.failed)
            }
            view.txtProgress.text =
                    context.getString(R.string.txt_progress, score, resultTestList.size)
            view.resultProgress.progress = score
            view.resultProgress.max = resultTestList.size
            return ViewHolder(view)
        }
        val view = LayoutInflater.from(context).inflate(R.layout.item_result, viewGroup, false)
        return ViewHolder(view)
    }

    private val score = calScore()

    override fun onBindViewHolder(itemViewHolder: ViewHolder, position: Int) {
        val view = itemViewHolder.itemView
        val context = view.context

        if (position > 0) {
            // skip 1 position for header
            val finalPosition = position - 1
            val resultTest = resultTestList[finalPosition]
            if (resultTest.userAnswer!!.isEmpty()) {
                view.visibility = View.GONE
            } else {
                view.txtQuestion.text = resultTest.question!!.replace(
                    "\\n ",
                    System.getProperty("line.separator")!!
                )
                view.txtUserAns.text =
                        context.getString(R.string.your_choose, resultTest.userAnswer)
                view.txtCorrectAns.text = context.getString(R.string.correct_ans, resultTest.answer)

                if (position == resultTestList.size && position >= Utils.ITEM_SHOW_ADS) {
                    val layoutNativeAds = view.findViewById<FrameLayout>(R.id.resultAdsContainerBottom)
                    if (isResult)
                        AdUtil.showNativeAdFbLarge(
                            context,
                            layoutNativeAds,
                            R.string.adsFb_ResultTestFragmentBottom,
                            R.string.adsGg_ResultTestFragmentBottom
                        )
                    else
                        AdUtil.showNativeAdFbLarge(
                            context,
                            layoutNativeAds,
                            R.string.adsFb_ResultTestFragmentBottom,
                            R.string.adsGg_ResultTestFragmentBottom
                        )
                }

                if (resultTest.answer != resultTest.userAnswer) {
                    itemViewHolder.itemView.txtUserAns.setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.red500
                        )
                    )
                    if (position == 1) {
                        itemViewHolder.itemView.txtHeader.text =
                                context.getString(R.string.wrong_ans)
                        itemViewHolder.itemView.txtHeader.visibility = View.VISIBLE
                    }
                } else {
                    view.txtCorrectAns.visibility = View.GONE
                    if (position == (resultTestList.size - score + 1)) {
                        itemViewHolder.itemView.txtHeader.text =
                                context.getString(R.string.right_ans)
                        itemViewHolder.itemView.txtHeader.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int = position


    private fun calScore(): Int {
        return resultTestList.count { it.userAnswer == it.answer }
    }

    // plus 1 because result have header
    override fun getItemCount(): Int = resultTestList.count { !it.userAnswer!!.isEmpty() } + 1

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
