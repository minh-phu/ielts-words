package com.ielts.vocabulary.be.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import com.huma.room_for_asset.RoomAsset
import com.ielts.vocabulary.be.data.database.AppDatabase.Companion.DATABASE_VERSION
import com.ielts.vocabulary.be.data.database.daos.LessonDao
import com.ielts.vocabulary.be.data.database.daos.WordDao
import com.ielts.vocabulary.be.data.database.entities.Lesson
import com.ielts.vocabulary.be.data.database.entities.Word


@Database(
    entities = [(Lesson::class), (Word::class)],
    version = DATABASE_VERSION,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun lessonDao(): LessonDao
    abstract fun wordDao(): WordDao

    companion object {
        const val DATABASE_VERSION = 2
        private const val DATABASE_NAME = "assets.db"

        private var sInstance: AppDatabase? = null
        @Synchronized
        fun getInstance(context: Context): AppDatabase {
            if (sInstance == null) {
                sInstance = RoomAsset
                    .databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        DATABASE_NAME
                    )
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()
            }
            return sInstance as AppDatabase
        }
    }
}