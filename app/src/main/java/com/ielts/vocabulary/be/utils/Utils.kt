package com.ielts.vocabulary.be.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.media.MediaPlayer
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.preference.PreferenceManager
import android.text.Html
import android.text.Spanned
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.ielts.vocabulary.be.R


object Utils {
    external fun getUtilsSecretKey(): String

    init {
        System.loadLibrary("native-lib")
    }

    const val ITEM_SHOW_ADS = 8

    fun checkUserRateApp(context: Context): Boolean {
        val sharedPreferences: SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getBoolean(Constants.CHECK_RATE, false)
    }

    fun getMediaCorrect(context: Context): MediaPlayer {
        val resIdSound =
            context.resources.getIdentifier(Constants.FILE_CORRECT, "raw", context.packageName)
        return MediaPlayer.create(context, resIdSound)
    }

    private var alertDialogRating: AlertDialog? = null

    fun openWebPage(activity: Activity, url: String) {
        try {
            val webPage = Uri.parse(url)
            val intent = Intent(Intent.ACTION_VIEW, webPage)
            if (intent.resolveActivity(activity.packageManager) != null) {
                activity.startActivity(intent)
            }
        } catch (e: java.lang.Exception) {

        }
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showDialogRate(activity: Activity) {
        val sharedPreferences: SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(activity)
        val dialogBuilder = androidx.appcompat.app.AlertDialog.Builder(activity)
        val dialogView = View.inflate(activity, R.layout.dialog_rating_app, null)
        dialogBuilder.setView(dialogView)

        dialogView.findViewById<TextView>(R.id.btnRatingApp).setOnClickListener {
            val marketIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=${activity.packageName}"))
            marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            activity.startActivity(marketIntent)
            val editor = sharedPreferences.edit()
            editor.putBoolean(Constants.CHECK_RATE, true)
            editor.apply()
            alertDialogRating?.dismiss()
        }
        dialogView.findViewById<TextView>(R.id.btnCancel).setOnClickListener {
            activity.finish()
        }
        alertDialogRating = dialogBuilder.create()

        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 80)
        alertDialogRating?.setCanceledOnTouchOutside(true)
        alertDialogRating?.window?.setBackgroundDrawable(inset)
        alertDialogRating?.show()
    }

    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null
    }

    @Suppress("DEPRECATION")
    fun fromHtml(html: String?): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(html)
        }
    }

}