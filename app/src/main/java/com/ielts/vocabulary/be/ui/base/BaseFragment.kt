package com.ielts.vocabulary.be.ui.base

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import com.ielts.vocabulary.be.ui.MainActivity

abstract class BaseFragment : Fragment(), BaseView {

    private var compositeDisposable: CompositeDisposable? = null

    private fun getComposite(): CompositeDisposable {
        if (compositeDisposable == null) {
            compositeDisposable = CompositeDisposable()
        }
        return compositeDisposable as CompositeDisposable
    }

    override fun getMyContext(): Context = requireContext()

    override fun addDispose(d: Disposable) {
        getComposite().add(d)
    }

    override fun onDestroy() {
        if (compositeDisposable != null) {
            compositeDisposable?.dispose()
        }
        super.onDestroy()
    }

    override fun onViewCreated(view: View, data: Bundle?) {
        super.onViewCreated(view, data)
        updateView()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (getMenuLayout() != 0) {
            setHasOptionsMenu(true)
        }
        return inflater.inflate(getFragmentLayout(), container, false)
    }

    fun setTopTitle(title: String) {
        (activity as MainActivity).setTopTitle(title)
    }

    fun closeFragment() {
        (activity as MainActivity).onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if (getMenuLayout() != 0) {
            inflater?.inflate(getMenuLayout(), menu)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    @MenuRes
    protected open fun getMenuLayout(): Int = 0

    @LayoutRes
    abstract fun getFragmentLayout(): Int

    abstract fun updateView()

    fun setEventClick(listOfView: List<View>, listener: MyOnClickListener?) {
        listOfView.iterator().forEach { it ->
            it.setOnClickListener {
                listener?.eventClick(it)
            }
        }
    }

    fun showMessage(
        message: String,
        duration: Int,
        actionName: String? = null,
        callback: CallbackSnackBar? = null
    ) {
        val content = activity?.findViewById<View>(android.R.id.content)
        content?.let { contentView ->
            val snackBar = Snackbar.make(contentView, message, duration)
            callback?.let {
                snackBar.setAction(actionName) {
                    callback.onAction()
                }
            }
            snackBar.show()
        }
    }

    interface MyOnClickListener {
        fun eventClick(v: View)
    }

    interface CallbackSnackBar {
        fun onAction()
    }
}

