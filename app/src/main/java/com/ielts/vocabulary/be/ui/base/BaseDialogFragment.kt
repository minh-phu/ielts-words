package com.ielts.vocabulary.be.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.DialogFragment
import io.reactivex.disposables.Disposable


abstract class BaseDialogFragment : DialogFragment(), BaseView {

    override fun addDispose(d: Disposable) {

    }

    override fun getMyContext(): Context = requireContext()


    override fun onViewCreated(view: View, data: Bundle?) {
        super.onViewCreated(view, data)
        updateView()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getFragmentLayout(), container, false)
    }

    @LayoutRes
    abstract fun getFragmentLayout(): Int

    abstract fun updateView()

}

