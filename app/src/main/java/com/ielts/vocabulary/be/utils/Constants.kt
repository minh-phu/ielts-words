package com.ielts.vocabulary.be.utils

object Constants {

    // share preference constant
    const val CHECK_PURCHASE = "is_purchase"
    const val CHECK_RATE = "is_rate"

    // sound constant
    const val FILE_CORRECT = "correct"
    const val FILE_WRONG = "wrong"

}
