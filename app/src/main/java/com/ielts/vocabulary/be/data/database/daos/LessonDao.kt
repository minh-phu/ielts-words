package com.ielts.vocabulary.be.data.database.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ielts.vocabulary.be.data.database.entities.Lesson


@Dao
interface LessonDao {

    @Query("SELECT * FROM Lesson")
    fun getAllLesson(): LiveData<List<Lesson>>

    @Query("SELECT * FROM Lesson WHERE id = :id LIMIT 1")
    fun getLessonById(id: Int): Lesson

    @Insert
    fun insertAll(lessonList: List<Lesson>)

    @Insert
    fun insert(lesson: Lesson)

    @Update
    fun update(lesson: Lesson)

    @Delete
    fun delete(lesson: Lesson)

}