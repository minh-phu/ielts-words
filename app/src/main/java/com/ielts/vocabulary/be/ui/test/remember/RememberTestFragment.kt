package com.ielts.vocabulary.be.ui.test.remember

import android.annotation.SuppressLint
import android.view.MenuItem
import android.view.View
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_test_remember.*
import com.ielts.vocabulary.be.R
import com.ielts.vocabulary.be.data.database.daos.WordDao
import com.ielts.vocabulary.be.data.database.entities.Lesson
import com.ielts.vocabulary.be.data.model.Question
import com.ielts.vocabulary.be.data.model.ResultPractice
import com.ielts.vocabulary.be.data.model.TestStyle
import com.ielts.vocabulary.be.ui.base.BaseFragment
import com.ielts.vocabulary.be.ui.resulttest.ResultTestDialogFragment
import com.ielts.vocabulary.be.utils.AdUtil
import com.ielts.vocabulary.be.utils.InjectorUtils
import java.util.*
import java.util.concurrent.TimeUnit

class RememberTestFragment : BaseFragment(), BaseFragment.MyOnClickListener,
    ResultTestDialogFragment.OnCloseResultTestDialogFragment {

    companion object {
        const val DELAY_REMEMBER_QUESTION = 2000L
    }

    private lateinit var wordDao: WordDao
    private var lessonId: Int = 0
    private lateinit var testStyle: String

    private var isClickable = true

    private var numberQuestionCurrent = -1
    private var score = 0
    private var listResult = ArrayList<ResultPractice>()
    private lateinit var mListQuestion: List<Question>

    override fun getFragmentLayout(): Int = R.layout.fragment_test_remember

    override fun getMenuLayout(): Int = R.menu.menu_result_test

    override fun updateView() {
        lessonId = RememberTestFragmentArgs.fromBundle(arguments!!)
            .lessonNumber
        testStyle = RememberTestFragmentArgs.fromBundle(
            arguments!!
        ).testStyle
        wordDao = InjectorUtils.provideWordDao(requireContext())
        AdUtil.showNativeAdsFbSmallInList(
            requireContext(),
            adsTestContainer,
            R.string.adsFb_TestRemember,
            R.string.adsGg_TestRemember
        )
        val listWord = wordDao.getWordsByLessonId(lessonId)
        mListQuestion = if (TestStyle.valueOf(testStyle) == TestStyle.VOCABULARY_TEST) {
            setTopTitle(getString(R.string.txt_test_vocabulary))
            WordDao.getDescQuestion(listWord)
        } else {
            setTopTitle(getString(R.string.txt_meaning_test))
            WordDao.getEnQuestion(listWord)
        }

        setQuestionUI(0)
        setEventClick(listOf(btnA, btnB, btnC, btnD), this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_show_result ->
                showResultTest(false)
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("SetTextI18n")
    private fun setQuestionUI(timeDelay: Long) {
        addDispose(Observable.timer(timeDelay, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (numberQuestionCurrent < mListQuestion.size - 1) {
                    btnA.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )
                    btnB.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )
                    btnC.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )
                    btnD.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )
                    isClickable = true
                    numberQuestionCurrent++
                    txtQuesNumber.text = "${(numberQuestionCurrent + 1)}/${mListQuestion.size}"

                    getCurrentQuestion().let { question ->
                        txtQuestion.text = getCurrentQuestion().question
                        txtA.text = question.answerSet[0].answerDecrypt
                        txtB.text = question.answerSet[1].answerDecrypt
                        txtC.text = question.answerSet[2].answerDecrypt
                        txtD.text = question.answerSet[3].answerDecrypt
                    }
                } else {
                    showResultTest(true)
                }
            })
    }

    override fun eventClick(v: View) {
        if (isClickable) {
            var result = ""
            when (v.id) {
                R.id.btnA -> result = txtA.text.toString()
                R.id.btnB -> result = txtB.text.toString()
                R.id.btnC -> result = txtC.text.toString()
                R.id.btnD -> result = txtD.text.toString()
            }
            addItemResult(result)
            if (checkAnswer(result)) {
                (v as CardView).setCardBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.green300
                    )
                )
            } else {
                (v as CardView).setCardBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.red300
                    )
                )
            }
            isClickable = false
            setQuestionUI(DELAY_REMEMBER_QUESTION)
        }
    }

    private fun showResultTest(isCompleteTest: Boolean) {
        when {
            listResult.size > 0 -> ResultTestDialogFragment
                .newInstance(listResult, isCompleteTest, this)
                .show(childFragmentManager, "ResultTestFragment")
            else -> showMessage(getString(R.string.txt_result_empty), Snackbar.LENGTH_SHORT)
        }
    }

    override fun onCompleteTest() {
        closeFragment()
    }

    override fun onPause() {
        updateLesson()
        super.onPause()
    }

    private fun getCurrentQuestion(): Question = mListQuestion[numberQuestionCurrent]

    private fun addItemResult(result: String) {
        val item = ResultPractice(
            getCurrentQuestion().question,
            getCurrentQuestion().correctAnswerDecrypt,
            result
        )
        listResult.add(item)
    }

    private fun checkAnswer(result: String): Boolean {
        if (result == getCurrentQuestion().correctAnswerDecrypt) {
            score++
            return true
        }
        return false
    }

    private fun updateLesson() {
        val lessonDao = InjectorUtils.provideLessonDao(requireContext())
        val lesson: Lesson = lessonDao.getLessonById(lessonId)
        when (testStyle) {
            TestStyle.VOCABULARY_TEST.name -> if (lesson.scoreRememberOne < score) {
                lesson.scoreRememberOne = score
                lessonDao.update(lesson)
            }
            TestStyle.MEANING_TEST.name -> if (lesson.scoreRememberTwo < score) {
                lesson.scoreRememberTwo = score
                lessonDao.update(lesson)
            }
        }
    }

}

