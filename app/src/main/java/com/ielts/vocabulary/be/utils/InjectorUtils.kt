package com.ielts.vocabulary.be.utils

import android.content.Context
import com.ielts.vocabulary.be.data.database.AppDatabase
import com.ielts.vocabulary.be.data.database.daos.LessonDao
import com.ielts.vocabulary.be.data.database.daos.WordDao
import com.ielts.vocabulary.be.data.database.repository.LessonRepository
import com.ielts.vocabulary.be.data.viewmodel.LessonViewModelFactory

object InjectorUtils {
    private fun getLessonRepository(context: Context): LessonRepository {
        return LessonRepository.getInstance(provideLessonDao(context))
    }

    fun provideLessonViewModelFactory(context: Context): LessonViewModelFactory {
        val repository = getLessonRepository(context)
        return LessonViewModelFactory(repository)
    }

    fun provideLessonDao(context: Context): LessonDao {
        return AppDatabase.getInstance(context).lessonDao()
    }

    fun provideWordDao(context: Context): WordDao {
        return AppDatabase.getInstance(context).wordDao()
    }
}
