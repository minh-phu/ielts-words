package com.ielts.vocabulary.be.ui.lesson.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.l4digital.fastscroll.FastScroller
import kotlinx.android.synthetic.main.item_lesson.view.*
import com.ielts.vocabulary.be.R
import com.ielts.vocabulary.be.data.database.entities.Lesson
import com.ielts.vocabulary.be.data.model.TestStyle
import com.ielts.vocabulary.be.utils.AdUtil

class LessonAdapter(private val lessonRecyclerClick: LessonAdapter.OnActionListLesson) :
    ListAdapter<Lesson, LessonAdapter.LessonViewHolder>(LessonDiffCallback()),
    FastScroller.SectionIndexer {

    companion object {
        const val ITEM_PER_ADS_LESSON = 2
        const val FIRST_ADS_POSITION = 1
    }

    override fun getSectionText(position: Int): CharSequence {
        return "Lesson " + (position + 1)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): LessonViewHolder {
        val itemView =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_lesson, viewGroup, false)
        val layoutNativeAds = itemView.findViewById<View>(R.id.adsTestContainer) as FrameLayout
        if ((i + FIRST_ADS_POSITION) % LessonAdapter.ITEM_PER_ADS_LESSON == 0) {
            AdUtil.showNativeAdsFbSmallInList(
                viewGroup.context,
                layoutNativeAds,
                R.string.adsFb_ListLesson,
                R.string.adsGg_ListLesson
            )
        }
        return LessonViewHolder(itemView)
    }

    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: LessonViewHolder, position: Int) {
        getItem(position).let { lesson ->
            with(holder) {
                val context = itemView.context
                itemView.tvCategoryVocabulary.text =
                        context.getString(R.string.lesson_name, position + 1)

                lesson.scoreRememberOne.let {
                    itemView.progressTestOne.progress = it.toFloat()
                }
                lesson.scoreRememberTwo.let {
                    itemView.progressTestTwo.progress = it.toFloat()
                }
                lesson.scoreListenOne.let {
                    itemView.progressListenTestOne.progress = it.toFloat()
                }
                lesson.scoreListenTwo.let {
                    itemView.progressListenTestTwo.progress = it.toFloat()
                }
            }
        }
    }

    inner class LessonViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        init {
            itemView.btnLearnWord.setOnClickListener {
                lessonRecyclerClick.onItemLessonClick(
                    getItem(adapterPosition),
                    TestStyle.VIEW_LESSON
                )
            }

            itemView.btnTestOne.setOnClickListener {
                lessonRecyclerClick.onItemLessonClick(
                    getItem(adapterPosition),
                    TestStyle.VOCABULARY_TEST
                )
            }
            itemView.btnTestTwo.setOnClickListener {
                lessonRecyclerClick.onItemLessonClick(
                    getItem(adapterPosition),
                    TestStyle.MEANING_TEST
                )
            }
            itemView.btnListenOne.setOnClickListener {
                lessonRecyclerClick.onItemLessonClick(
                    getItem(adapterPosition),
                    TestStyle.CHOOSE_PRONOUNCE
                )
            }
            itemView.btnListenTwo.setOnClickListener {
                lessonRecyclerClick.onItemLessonClick(
                    getItem(adapterPosition),
                    TestStyle.LISTEN_AND_CHOOSE
                )
            }
        }
    }

    interface OnActionListLesson {
        fun onItemLessonClick(lesson: Lesson, style: TestStyle)
    }

    class LessonDiffCallback : DiffUtil.ItemCallback<Lesson>() {

        override fun areItemsTheSame(
            oldItem: Lesson,
            newItem: Lesson
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: Lesson,
            newItem: Lesson
        ): Boolean {
            if (oldItem.scoreRememberOne == newItem.scoreRememberOne
                && oldItem.scoreRememberTwo == newItem.scoreRememberTwo
                && oldItem.scoreListenOne == newItem.scoreListenOne
                && oldItem.scoreListenTwo == newItem.scoreListenTwo
            ) {
                return true
            }
            return false
        }
    }
}