package com.ielts.vocabulary.be.data.model


enum class TestStyle {
    VIEW_LESSON, MEANING_TEST, VOCABULARY_TEST, CHOOSE_PRONOUNCE, LISTEN_AND_CHOOSE
}