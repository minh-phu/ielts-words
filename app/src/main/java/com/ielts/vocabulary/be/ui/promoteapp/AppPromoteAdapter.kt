package com.ielts.vocabulary.be.ui.promoteapp

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_promote_app.view.*
import com.ielts.vocabulary.be.R

class AppPromoteAdapter(
    private val listAppPromote: ArrayList<AppPromote>, private val listener: OnPromoteClick
) : RecyclerView.Adapter<AppPromoteAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_promote_app, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = listAppPromote[position]
        with(holder.itemView) {
            if (item.isMoreApp) {
                tvAppName.setTypeface(null, Typeface.BOLD)
                tvAppName.setTextColor(ContextCompat.getColor(context,R.color.black))
            }
            tvAppName.text = item.appName
            imgIconApp.setImageDrawable(ContextCompat.getDrawable(context, item.appIcon))
            setOnClickListener {
                listener.onItemPromoteClick(item)
            }
        }
    }

    override fun getItemViewType(position: Int): Int = position

    override fun getItemCount(): Int = listAppPromote.size


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface OnPromoteClick {
        fun onItemPromoteClick(item: AppPromote)
    }
}
