package com.ielts.vocabulary.be.utils.mediaplayer

import android.content.Context
import android.media.MediaPlayer

class MediaPlayerManager {

    private var mediaPlayer: MediaPlayer? = null

    companion object {
        private var manager: MediaPlayerManager? = null

        val instance: MediaPlayerManager
            get() {
                if (manager == null) {
                    manager = MediaPlayerManager()
                }
                return manager as MediaPlayerManager
            }
    }

    @JvmOverloads
    fun play(
        context: Context, resId: Int
        , completionListener: MediaPlayer.OnCompletionListener? = null
        , errorListener: MediaPlayer.OnErrorListener? = null
        , myMediaPlayerListener: MyMediaPlayerListener? = null
    ) {
        if (mediaPlayer != null) {
            releaseMediaPlayer()
        }

        mediaPlayer = MediaPlayer.create(context, resId)
        //mediaPlayer?.setOnCompletionListener { arg0 -> arg0.release() }
        mediaPlayer?.setOnCompletionListener(completionListener)
        mediaPlayer?.setOnErrorListener(errorListener)
        mediaPlayer?.setOnPreparedListener { player ->
            player.start()
            myMediaPlayerListener?.onPrepared(player)
        }

        startMediaPlay()
    }

    @JvmOverloads
    fun play(
        context: Context, fileName: String
        , completionListener: MediaPlayer.OnCompletionListener? = null
        , errorListener: MediaPlayer.OnErrorListener? = null
        , myMediaPlayerListener: MyMediaPlayerListener? = null
    ) {
        val resIdSound = context.resources.getIdentifier(fileName, "raw", context.packageName)

        if (resIdSound != 0) {
            if (mediaPlayer != null) {
                releaseMediaPlayer()
            }

            mediaPlayer = MediaPlayer.create(context, resIdSound)
            //mediaPlayer?.setOnCompletionListener { arg0 -> arg0.release() }
            mediaPlayer?.setOnCompletionListener(completionListener)
            mediaPlayer?.setOnErrorListener(errorListener)
            mediaPlayer?.setOnPreparedListener { player ->
                player.start()
                myMediaPlayerListener?.onPrepared(player)
            }

            startMediaPlay()
        }

    }

    private fun releaseMediaPlayer() {
        try {
            if (mediaPlayer?.isPlaying == true) {
                mediaPlayer?.stop()
            }
            mediaPlayer?.release()
            mediaPlayer = null
        } catch (e: Exception) {
            //Log.e("MediaPlayer", "error " + e.message)
        }
    }

    private fun startMediaPlay() {
        try {
            mediaPlayer?.start()
        } catch (e: Exception) {
            //Log.e("MediaPlayer", "error " + e.message)
        }
    }

    interface MyMediaPlayerListener {
        fun onPrepared(player: MediaPlayer)
    }
}