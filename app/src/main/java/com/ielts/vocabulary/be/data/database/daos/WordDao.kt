package com.ielts.vocabulary.be.data.database.daos

import androidx.room.*
import com.ielts.vocabulary.be.data.database.entities.Word
import com.ielts.vocabulary.be.data.model.Question
import java.util.*


@Dao
interface WordDao {

    @Query("SELECT * FROM Word WHERE lesson_id = :lesson_id")
    fun getWordsByLessonId(lesson_id: Int): List<Word>

    @Query("SELECT * FROM Word ")
    fun getAllWords(): List<Word>

    @Query("SELECT * FROM Word WHERE description = :correctAnswer OR en = :correctAnswer LIMIT 1")
    fun getWordFromAnswer(correctAnswer: String): Word

    @Insert
    fun insertAll(words: List<Word>)

    @Insert
    fun insert(word: Word)

    @Update
    fun update(word: Word)

    @Delete
    fun delete(word: Word)

    companion object {
        fun getEnQuestion(wordList: List<Word>): List<Question> {

            val questionList = ArrayList<Question>()

            for (i in wordList.indices) {
                val word = wordList[i]
                val question = Question(word.descDecrypt, word.en!!, word.enDecrypt)

                for (index in randomVocabulary(i, wordList.size, 4))
                    question.addAnswerSet(wordList[index].en!!, wordList[index].enDecrypt)

                question.answerSet.shuffle()
                questionList.add(question)
            }

            questionList.shuffle()
            return questionList
        }

        fun getDescQuestion(wordList: List<Word>): List<Question> {

            val questionList = ArrayList<Question>()

            for (i in wordList.indices) {
                val word = wordList[i]
                val question = Question(word.enDecrypt, word.description!!, word.descDecrypt)

                for (index in randomVocabulary(i, wordList.size, 4))
                    question.addAnswerSet(
                        wordList[index].description!!,
                        wordList[index].descDecrypt
                    )

                question.answerSet.shuffle()
                questionList.add(question)
            }

            questionList.shuffle()
            return questionList
        }

        fun getAudioQuestion(wordList: List<Word>): ArrayList<Question> {
            val questionList = ArrayList<Question>()
            for (i in wordList.indices) {
                val vocabulary = wordList[i]
                val question =
                    Question(vocabulary.enDecrypt, vocabulary.description!!, vocabulary.descDecrypt)
                for (index in randomVocabulary(i, wordList.size, 4)) {
                    question.addAnswerSet(
                        wordList[index].description!!,
                        wordList[index].descDecrypt
                    )
                }
                question.answerSet.shuffle()
                questionList.add(question)
            }

            questionList.shuffle()
            return questionList

        }

        private fun randomVocabulary(
            questionIndex: Int,
            size: Int,
            questionSum: Int
        ): ArrayList<Int> {
            val randomGenerator = Random()
            val randomList = ArrayList<Int>()
            randomList.add(questionIndex)

            while (randomList.size < questionSum) {
                val random = randomGenerator.nextInt(size)
                if (!randomList.contains(random)) {
                    randomList.add(random)
                }
            }

            return randomList
        }
    }


}