package com.ielts.vocabulary.be.ui.test.choosePronounce

import android.annotation.SuppressLint
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_listen_test_one.*
import com.ielts.vocabulary.be.R
import com.ielts.vocabulary.be.data.database.daos.WordDao
import com.ielts.vocabulary.be.data.database.entities.Lesson
import com.ielts.vocabulary.be.data.database.entities.Word
import com.ielts.vocabulary.be.data.model.Question
import com.ielts.vocabulary.be.data.model.ResultPractice
import com.ielts.vocabulary.be.ui.base.BaseFragment
import com.ielts.vocabulary.be.ui.resulttest.ResultTestDialogFragment
import com.ielts.vocabulary.be.utils.AdUtil
import com.ielts.vocabulary.be.utils.InjectorUtils
import com.ielts.vocabulary.be.utils.Utils
import com.ielts.vocabulary.be.utils.tts.TextToSpeechManager
import java.util.ArrayList
import java.util.concurrent.TimeUnit

class ChoosePronounceFragment : BaseFragment(), BaseFragment.MyOnClickListener,
    ResultTestDialogFragment.OnCloseResultTestDialogFragment {


    companion object {
        const val TIME_DELAY_CHOOSE_PRONOUNCE = 2000
    }

    private lateinit var wordDao: WordDao
    private var lessonId: Int = 0
    private lateinit var mListQuestion: List<Question>

    private lateinit var listBtnAnswer: List<View>
    private var score = 0
    private var numberQuestionCurrent = -1
    private var answerCurrent = -1
    private lateinit var questionCurrent: Question
    private var listResult = ArrayList<ResultPractice>()

    override fun getFragmentLayout(): Int = R.layout.fragment_listen_test_one

    override fun getMenuLayout(): Int = R.menu.menu_result_test

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_show_result ->
                showResultTest(false)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showResultTest(isCompleteTest: Boolean) {
        when {
            listResult.size > 0 -> ResultTestDialogFragment
                .newInstance(listResult, isCompleteTest, this)
                .show(childFragmentManager, "ResultTestFragment")
            else -> showMessage(getString(R.string.txt_result_empty), Snackbar.LENGTH_SHORT)
        }
    }

    override fun onCompleteTest() {
        closeFragment()
    }

    override fun updateView() {
        listBtnAnswer = listOf(btnSpeakOne, btnSpeakTwo, btnSpeakThree, btnSpeakFour)
        lessonId = ChoosePronounceFragmentArgs.fromBundle(arguments!!).lessonNumber
        wordDao = InjectorUtils.provideWordDao(requireContext())
        val listWord = wordDao.getWordsByLessonId(lessonId)
        mListQuestion = WordDao.getEnQuestion(listWord)
        AdUtil.showNativeAdsFbSmallInList(
            requireContext(),
            adsTestContainer,
            R.string.adsFb_TestListen1,
            R.string.adsGg_TestListen1
        )
        setUpListenTestUI(0)
    }

    // typeCheck = true - play sound & Set up UI
    // else - play sound
    private fun playSoundAnswer(word: Word, typeCheck: Boolean) {
        TextToSpeechManager.playText(requireContext(), word.enDecrypt)
        if (typeCheck) {
            setUpListenTestUI(TIME_DELAY_CHOOSE_PRONOUNCE)
        }
    }

    private fun enableButtonSpeak() {
        setEventClick(
            listOf(btnSpeakOne, btnSpeakTwo, btnSpeakThree, btnSpeakFour, btnSubmitTest),
            this
        )
        btnSpeakOne.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.background_speaker)
        btnSpeakTwo.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.background_speaker)
        btnSpeakThree.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.background_speaker)
        btnSpeakFour.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.background_speaker)
    }

    private fun disableButtonSpeak() {
        setEventClick(
            listOf(btnSpeakOne, btnSpeakTwo, btnSpeakThree, btnSpeakFour, btnSubmitTest),
            null
        )
    }

    @SuppressLint("SetTextI18n")
    private fun setUpListenTestUI(duration: Int) {
        addDispose(Observable.timer(duration.toLong(), TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (numberQuestionCurrent < mListQuestion.size - 1) {
                    enableButtonSpeak()
                    answerCurrent = -1
                    btnSubmitTest.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.colorHighLight
                        )
                    )
                    numberQuestionCurrent++

                    tvNumberQuestion.text = "${(numberQuestionCurrent + 1)}/${mListQuestion.size}"
                    questionCurrent = mListQuestion[numberQuestionCurrent]
                    tvQuestion.text = questionCurrent.question

                } else {
                    showResultTest(true)
                }
            })
    }

    private fun updateBtnAnswer(idView: Int) {
        for (btn: View in listBtnAnswer) {
            if (btn.id == idView) {
                btn.background = ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.background_speaker_active
                )
            } else {
                btn.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.background_speaker)
            }
        }
    }

    override fun eventClick(v: View) {
        val listAnswerSet = questionCurrent.answerSet
        when (v.id) {
            R.id.btnSpeakOne, R.id.btnSpeakTwo, R.id.btnSpeakThree, R.id.btnSpeakFour -> {
                when (v.id) {
                    R.id.btnSpeakOne -> answerCurrent = 0
                    R.id.btnSpeakTwo -> answerCurrent = 1
                    R.id.btnSpeakThree -> answerCurrent = 2
                    R.id.btnSpeakFour -> answerCurrent = 3
                }
                btnSubmitTest.setCardBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.accent
                    )
                )
                updateBtnAnswer(v.id)
                val word =
                    wordDao.getWordFromAnswer(questionCurrent.answerSet[answerCurrent].answer)
                playSoundAnswer(word, false)

            }
            R.id.btnSubmitTest -> if (answerCurrent != -1) {


                disableButtonSpeak()
                val correctAnswerDecrypt = questionCurrent.correctAnswerDecrypt

                val item = ResultPractice(
                    questionCurrent.question,
                    correctAnswerDecrypt,
                    listAnswerSet[answerCurrent].answerDecrypt
                )

                listResult.add(item)
                if (listAnswerSet[answerCurrent].answerDecrypt == correctAnswerDecrypt) {
                    score++
                    playSoundCheck()
                } else {
                    val answer = questionCurrent.correctAnswer
                    val word = wordDao.getWordFromAnswer(answer)
                    playSoundAnswer(word, true)
                }
                showCorrectAnswer()
            }
        }
    }

    private fun playSoundCheck() {
        val mediaPlayer = Utils.getMediaCorrect(requireContext())

        mediaPlayer.setOnPreparedListener { player ->
            setUpListenTestUI(player.duration)
            player.start()
        }
        mediaPlayer.setOnCompletionListener { arg0 -> arg0.release() }
    }

    private fun showCorrectAnswer() {
        var indexCorrect = 0
        for (i in 0 until questionCurrent.answerSet.size) {
            if (questionCurrent.correctAnswer == questionCurrent.answerSet[i].answer) {
                indexCorrect = i
            }
        }
        when (indexCorrect) {
            0 -> btnSpeakOne.background = ContextCompat.getDrawable(
                requireContext(),
                R.drawable.background_speaker_correct
            )
            1 -> btnSpeakTwo.background = ContextCompat.getDrawable(
                requireContext(),
                R.drawable.background_speaker_correct
            )
            2 -> btnSpeakThree.background = ContextCompat.getDrawable(
                requireContext(),
                R.drawable.background_speaker_correct
            )
            3 -> btnSpeakFour.background = ContextCompat.getDrawable(
                requireContext(),
                R.drawable.background_speaker_correct
            )
        }
    }

    override fun onPause() {
        updateLesson()
        super.onPause()
    }

    private fun updateLesson() {
        val lessonDao = InjectorUtils.provideLessonDao(requireContext())
        val lesson: Lesson = lessonDao.getLessonById(lessonId)
        if (lesson.scoreListenOne < score) {
            lesson.scoreListenOne = score
            lessonDao.update(lesson)
        }
    }
}
