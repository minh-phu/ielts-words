package com.ielts.vocabulary.be.ui.test.listenChoose

import android.annotation.SuppressLint
import android.view.MenuItem
import android.view.View
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_listen_test_two.*
import com.ielts.vocabulary.be.R
import com.ielts.vocabulary.be.data.database.daos.WordDao
import com.ielts.vocabulary.be.data.database.entities.Lesson
import com.ielts.vocabulary.be.data.model.Question
import com.ielts.vocabulary.be.data.model.ResultPractice
import com.ielts.vocabulary.be.ui.base.BaseFragment
import com.ielts.vocabulary.be.ui.resulttest.ResultTestDialogFragment
import com.ielts.vocabulary.be.utils.AdUtil
import com.ielts.vocabulary.be.utils.InjectorUtils
import com.ielts.vocabulary.be.utils.tts.TextToSpeechManager
import java.util.ArrayList
import java.util.concurrent.TimeUnit

class ListenAndChooseFragment : BaseFragment(), BaseFragment.MyOnClickListener,
    ResultTestDialogFragment.OnCloseResultTestDialogFragment {

    override fun onCompleteTest() {
        closeFragment()
    }

    companion object {
        const val DELAY_LISTEN_TWO_QUESTION = 2000L
    }

    private var isClickable = true

    private var score = 0
    private lateinit var questionCurrent: Question
    private var numberQuestionCurrent = -1

    private lateinit var wordDao: WordDao
    private var lessonId: Int = 0
    private lateinit var mListQuestion: List<Question>
    private var listResult = ArrayList<ResultPractice>()

    override fun getFragmentLayout(): Int = R.layout.fragment_listen_test_two

    override fun getMenuLayout(): Int = R.menu.menu_result_test

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_show_result ->
                showResultTest(false)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showResultTest(isCompleteTest: Boolean) {
        when {
            listResult.size > 0 -> ResultTestDialogFragment
                .newInstance(listResult, isCompleteTest, this)
                .show(childFragmentManager, "ResultTestFragment")
            else -> showMessage(getString(R.string.txt_result_empty), Snackbar.LENGTH_SHORT)
        }
    }

    override fun updateView() {
        lessonId = ListenAndChooseFragmentArgs.fromBundle(arguments!!).lessonNumber
        wordDao = InjectorUtils.provideWordDao(requireContext())
        AdUtil.showNativeAdsFbSmallInList(
            requireContext(),
            adsTestContainer,
            R.string.adsFb_TestListen2,
            R.string.adsGg_TestListen2
        )
        setEventClick(
            listOf(
                btnAnswerOne,
                btnAnswerTwo,
                btnAnswerThree,
                btnAnswerFour,
                imgQuestionSound
            ), this
        )
        val listWord = wordDao.getWordsByLessonId(lessonId)
        mListQuestion = WordDao.getAudioQuestion(listWord)
        setQuestionUI(0)
    }

    override fun eventClick(v: View) {
        when (v.id) {
            R.id.btnAnswerOne, R.id.btnAnswerTwo, R.id.btnAnswerThree, R.id.btnAnswerFour -> if (isClickable) {
                val result: String = when (v.id) {
                    R.id.btnAnswerOne -> tvAnswerOne.text.toString()
                    R.id.btnAnswerTwo -> tvAnswerTwo.text.toString()
                    R.id.btnAnswerThree -> tvAnswerThree.text.toString()
                    else -> tvAnswerFour.text.toString()
                }
                val item = ResultPractice(
                    questionCurrent.question,
                    questionCurrent.correctAnswerDecrypt,
                    result
                )
                listResult.add(item)
                if (result == questionCurrent.correctAnswerDecrypt) {
                    score++
                    (v as CardView).setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.green300
                        )
                    )
                } else {
                    (v as CardView).setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.red300
                        )
                    )
                }
                isClickable = false
                setQuestionUI(DELAY_LISTEN_TWO_QUESTION)

            }
            R.id.imgQuestionSound -> questionCurrent.correctAnswer.let { answer ->
                val word = wordDao.getWordFromAnswer(answer)
                TextToSpeechManager.playText(requireContext(), word.enDecrypt)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setQuestionUI(timeDelay: Long) {
        addDispose(Observable.timer(timeDelay, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (numberQuestionCurrent < mListQuestion.size - 1) {

                    isClickable = true
                    btnAnswerOne.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )
                    btnAnswerTwo.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )
                    btnAnswerThree.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )
                    btnAnswerFour.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )

                    numberQuestionCurrent++
                    tvNumberQuestion.text = "${(numberQuestionCurrent + 1)}/${mListQuestion.size}"

                    questionCurrent = mListQuestion[numberQuestionCurrent]

                    questionCurrent.apply {
                        tvAnswerOne.text = this.answerSet[0].answerDecrypt
                        tvAnswerTwo.text = this.answerSet[1].answerDecrypt
                        tvAnswerThree.text = this.answerSet[2].answerDecrypt
                        tvAnswerFour.text = this.answerSet[3].answerDecrypt
                    }

                    addDispose(Observable.timer(500, TimeUnit.MILLISECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            questionCurrent.let { question ->
                                val word = wordDao.getWordFromAnswer(question.correctAnswer)
                                TextToSpeechManager.playText(
                                    requireContext(),
                                    word.enDecrypt
                                )
                            }
                        })
                } else {
                    showResultTest(true)
                }
            })
    }

    override fun onPause() {
        updateLesson()
        super.onPause()
    }

    private fun updateLesson() {
        val lessonDao = InjectorUtils.provideLessonDao(requireContext())
        val lesson: Lesson = lessonDao.getLessonById(lessonId)
        if (lesson.scoreListenTwo < score) {
            lesson.scoreListenTwo = score
            lessonDao.update(lesson)
        }
    }

}
