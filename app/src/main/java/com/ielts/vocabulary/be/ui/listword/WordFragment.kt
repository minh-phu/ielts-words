package com.ielts.vocabulary.be.ui.listword

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.speech.RecognizerIntent
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_word.*
import com.ielts.vocabulary.be.R
import com.ielts.vocabulary.be.data.database.entities.Word
import com.ielts.vocabulary.be.ui.base.BaseFragment
import com.ielts.vocabulary.be.utils.AdUtil
import com.ielts.vocabulary.be.utils.Constants
import com.ielts.vocabulary.be.utils.Utils
import com.ielts.vocabulary.be.utils.mediaplayer.MediaPlayerManager
import com.ielts.vocabulary.be.utils.tts.TextToSpeechManager
import java.util.*

class WordFragment : BaseFragment(), BaseFragment.MyOnClickListener {

    lateinit var word: Word

    companion object {

        const val REQ_CODE_SPEECH_INPUT = 100

        fun newInstance(word: Word): WordFragment {
            val fragmentWord = WordFragment()
            fragmentWord.word = word
            return fragmentWord
        }
    }

    override fun getFragmentLayout(): Int = R.layout.fragment_word

    override fun updateView() {
        AdUtil.showNativeAdFbSmall(
            requireContext(),
            adsContainer,
            R.string.adsFb_ListWord,
            R.string.adsGg_ListWord
        )

        tvVocabulary.text = Utils.fromHtml(word.enDecrypt)
        tvMeaning.text = Utils.fromHtml("<b>Meaning</b> : " + word.descDecrypt)
        tvExample.text = Utils.fromHtml("<b>Example</b> : " + word.examDecrypt)
        setEventClick(listOf(btnSpeakVocabulary, btnOpenVoice), this)
    }

    override fun eventClick(v: View) {
        when (v.id) {
            R.id.btnSpeakVocabulary ->
                TextToSpeechManager.playText(requireContext(), word.enDecrypt)
            R.id.btnOpenVoice -> openVoice()
        }
    }

    private fun openVoice() {
        try {
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
            intent.putExtra(
                RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
            )
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
            intent.putExtra(
                RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt)
            )
            try {
                startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
            } catch (a: ActivityNotFoundException) {
                Toast.makeText(
                    activity!!.applicationContext,
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT
                ).show()
            }

        } catch (e: NullPointerException) {
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQ_CODE_SPEECH_INPUT -> {
                try {
                    if (resultCode == Activity.RESULT_OK && null != data) {
                        val result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                        if (result[0].toLowerCase() == word.enDecrypt.toLowerCase()) {
                            MediaPlayerManager.instance.play(
                                requireContext(),
                                Constants.FILE_CORRECT
                            )
                        } else {
                            MediaPlayerManager.instance.play(requireContext(), Constants.FILE_WRONG)
                            val message = getString(R.string.txt_result) + " : " + result[0]
                            val actionName = getString(R.string.txt_try_again)
                            showMessage(
                                message,
                                Snackbar.LENGTH_LONG,
                                actionName,
                                object : CallbackSnackBar {
                                    override fun onAction() {
                                        openVoice()
                                    }
                                })
                        }
                    }
                } catch (ignored: NullPointerException) {
                }

            }
        }
    }

}
