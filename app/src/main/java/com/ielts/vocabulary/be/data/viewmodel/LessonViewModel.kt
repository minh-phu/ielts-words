package com.ielts.vocabulary.be.data.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.ielts.vocabulary.be.data.database.entities.Lesson
import com.ielts.vocabulary.be.data.database.repository.LessonRepository

class LessonViewModel(lessonRepository: LessonRepository) : ViewModel() {

    var mListLesson: LiveData<List<Lesson>> = lessonRepository.getListLesson()

}