package com.ielts.vocabulary.be

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.google.android.gms.ads.MobileAds
import com.ielts.vocabulary.be.utils.CipherHelper
import com.ielts.vocabulary.be.utils.tts.TextToSpeechManager
import io.fabric.sdk.android.Fabric

class MyApplication : MultiDexApplication() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        MobileAds.initialize(applicationContext, getString(R.string.app_ads_id))
        TextToSpeechManager.getInstance(this)
        CipherHelper(getSecretKey())

        Fabric.with(this, Crashlytics())
        Fabric.with(this, Answers())
    }

    private external fun getSecretKey(): String

    companion object {
        init {
            System.loadLibrary("native-lib")
        }
    }
}
